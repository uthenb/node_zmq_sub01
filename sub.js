/*
var zmq = require('zmq')
var subscriber = zmq.socket('sub')
var client = zmq.socket('req')

subscriber.on('message', function(reply) {
  console.log('Received message: ', reply.toString());
})

subscriber.connect('tcp://localhost:8688')
subscriber.subscribe('')

client.connect('tcp://localhost:8888')
client.send('SYNC')

process.on('SIGINT', function() {
  subscriber.close()
  client.close()
})
*/
var zmq = require('zmq'), sock = zmq.socket('sub');

sock.connect('tcp://127.0.0.1:3000');
sock.subscribe('order/investing');
console.log('Subscriber connected to port 3000');

sock.on('message', function(topic, message) {
  console.log('received a message related to:', topic, 'containing message:', message);
});